package com.example.user.cansudemo;

import android.content.Context;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.effect.Effect;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.SystemClock;
import android.os.Vibrator;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Selection;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.SoundEffectConstants;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.lang.reflect.Field;

import static android.content.Context.VIBRATOR_SERVICE;

public class MainActivity extends AppCompatActivity {
    Handler handle=null;
    Runnable runnable=null;
    int eleman;
    int zaman;
    EditText text;
    //butonların tutulduğu arrayler
    public int[] butArray = {R.id.n1, R.id.n2, R.id.n3, R.id.n4, R.id.n5,R.id.n6,R.id.n7,R.id.n8,R.id.n9,R.id.n10,R.id.n11,R.id.n12,R.id.n13,R.id.n14,R.id.n15,R.id.n16,R.id.n17,R.id.n18,R.id.n19,R.id.n20,R.id.n21,R.id.n22,R.id.n23,R.id.d0,R.id.d1,R.id.d2,R.id.d3,R.id.d4,R.id.d5,R.id.d6,R.id.d7,R.id.d8,R.id.d9};
    public Button[] bt= new Button[butArray.length];
    //klavyede olacak tuşlar
    public String [] myArray = {"A", "B", "C", "D", "E", "F", "G", "H","İ","J","K","L","M","N","O","P","R","S","T","U","V","Y","Z","0","1","2","3","4","5","6","7","8","9"};


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Tuşlara basıldığında titreşim ekleme
        final Vibrator titresim= (Vibrator) MainActivity.this.getSystemService(Context.VIBRATOR_SERVICE);
        //Tuşlara basıldığında click sesi
        final MediaPlayer buttonSound = MediaPlayer.create(this,R.raw.o);
        //Yazılan text
        text=(EditText) findViewById(R.id.Text1);
        text.setTextSize(TypedValue.COMPLEX_UNIT_DIP,18);


        //Silme tuşları
        Button clear=(Button) findViewById(R.id.clr);
        Button delete=(Button) findViewById(R.id.del);
        Button space =(Button) findViewById(R.id.space);

        eleman=0;
        zaman = 100;
        handle = new Handler();
        runnable = new Runnable() {

            @Override
            public void run() {
                if (zaman != 0) {

                    zaman--;
                } else {
                    handle.removeCallbacks(runnable);
                    bt[eleman].setBackgroundColor(Color.GREEN);
                    eleman++;
                    if(eleman>butArray.length){
                        zaman++;
                    }
                }
                handle.postDelayed(runnable, 1000);

            }
        };
        runnable.run();
        Runnable updateTimerThread = new Runnable()
        {
            public void run()
            {
                /
                ll.setBackgroundColor(Color.BLACK);

            }else{
            ll.setBackgroundColor(Color.WHITE);
            color = 1;
        }

            handle.postDelayed(this, 5000);// will repeat after 5 seconds
        }
    };
    clear.setOnClickListener(new View.OnClickListener(){

        @Override
        public void onClick(View v) {

            if (text.getText().toString().matches("")){
                text.setText("");

            } else {
                //yazılan tüm bilgileri siler
                text.setText("");
            }


        }
    });
    delete.setOnClickListener(new View.OnClickListener(){

        @Override
        public void onClick(View v) {
            //edittext boş ise yine boş kalsı
            if(text.getText().toString().matches("")){
                text.setText("");
            }
            //eğer yazılmış text varsa en sondaki karakteri sil
            else {
                text.setText(text.getText().delete(text.getText().length() - 1, text.getText().length()));
            }
            /// titresim.vibrate(80);
        }
    });

    space.setOnClickListener(new View.OnClickListener(){
        //Boşluk karakteri ekler
        @Override
        public void onClick(View v) {
            text.setText(text.getText().insert(text.getText().length(), " "));
        }
    });
    //int j=0;
    //Silme tuşları hariç diğer tuşlar için
    for(int i = 0; i<butArray.length; i++)
    {
        final int b=i;
        //final int c=j;
        bt[b] = (Button)findViewById(butArray[b]);

            /*bt[c].setTextColor(Color.GREEN);
            bt[c].animate().setDuration(2000).withEndAction(new Runnable() {
                @Override
                public void run() {
                    // set color back to normal
                    bt[c].setTextColor(Color.BLACK);
                }
            }).start();*/
        bt[b].setOnClickListener(new View.OnClickListener() {

            //Tıklandığında
            @Override
            public void onClick(View v) {

                //Basılan harfler-sayılar pop up şeklinde görünüyor
                Toast.makeText(getApplicationContext(),myArray[b],Toast.LENGTH_SHORT).show();
                //titresim.vibrate(80);
                buttonSound.start();
                switch(v.getId()) {
                    //butonların id lerine göre
                    case R.id.n1:
                        //Eğer A butonuna basılırsa Edittext kısmına A yaz
                        text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                        break;
                    case R.id.n2:
                        text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                        break;
                    case R.id.n3:
                        text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                        break;
                    case R.id.n4:
                        text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                        break;
                    case R.id.n5:
                        text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                        break;
                    case R.id.n6:
                        text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                        break;
                    case R.id.n7:
                        text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                        break;
                    case R.id.n8:
                        text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                        break;
                    case R.id.n9:
                        text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                        break;
                    case R.id.n10:
                        text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                        break;
                    case R.id.n11:
                        text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                        break;
                    case R.id.n12:
                        text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                        break;
                    case R.id.n13:
                        text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                        break;
                    case R.id.n14:
                        text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                        break;
                    case R.id.n15:
                        text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                        break;

                    case R.id.n16:
                        text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                        break;
                    case R.id.n17:
                        text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                        break;
                    case R.id.n18:
                        text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                        break;
                    case R.id.n19:
                        text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                        break;
                    case R.id.n20:
                        text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                        break;
                    case R.id.n21:
                        text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                        break;
                    case R.id.n22:
                        text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                        break;
                    case R.id.n23:
                        text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                        break;

                    case R.id.d0:
                        text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                        break;
                    case R.id.d1:
                        text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                        break;
                    case R.id.d2:
                        text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                        break;
                    case R.id.d3:
                        text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                        break;
                    case R.id.d4:
                        text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                        break;
                    case R.id.d5:
                        text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                        break;
                    case R.id.d6:
                        text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                        break;
                    case R.id.d7:
                        text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                        break;
                    case R.id.d8:
                        text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                        break;
                    case R.id.d9:
                        text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                        break;

                }

            }
        });
        //j=j+1;
    }
}


}
